This program is an emulator of a service remote control for televisions.
Copyright (c) 2021 Maxim Sayshin. Email: sayrhin@gmail.com
Telegram: @NewSimpleton
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND.
The author warns that this is a research code, not a production tool.


this program is developed by linux ubuntu, mint
to start, go to the program directory in the terminal and run the command
command:
    ./remote_control
in this case, the arduino board must already be connected to the computer
the sketch IR_DA_TX.ino should be written in the arduino board
if there are problems with connecting the arduino board, you need to check its status by entering the command
command:
    lsusb
output example:
    Bus 002 Device 007: ID 1a86:7523 QinHeng Electronics HL-340 USB-Serial adapter
if this message appears, it is necessary to check whether the user is a member of the group dialout
if not, then add yourself to this group
command:
    useradd -G dialout new_user
We use
//============================================================================
Эта программа является эмулятором сервисного пульта дистанционного управления для телевизоров.
Электронная почта: sayrhin@gmail.com
Телеграмм: @NewSimpleton

ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ "КАК ЕСТЬ", БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ.
Автор предупреждает, что это исследовательский код, а не производственный инструмент.


эта программа разработана под linux ubuntu, mint
для запуска перейдите в каталог с программой в терминале и выполните команду 

./remote_control

в этом случае плата arduino уже должна быть подключена к компьютеру
с залитым скетчем IR_DA_TX.ino и ИК светодиодом
если возникли проблемы с подключением платы arduino, вам необходимо проверить ее состояние, введя команду command

lsusb

пример вывода 
Bus 002 Device 007: ID 1a86:7523 QinHeng Electronics HL-340 USB-Serial adapter

если появляется это сообщение, необходимо проверить, является ли пользователь членом группы dialout
если нет, то добавьте себя в эту группу

useradd -G dialout new_user

//===============================================================================
скетч IR_DA_RX.ino для считывания кодов с пультов. для этого ардуину с ИК приемником подключаем к компу 
и в терминале порта смотрим коды нажатых кнопок
//===============================================================================
БОЛЬШАЯ ПРОСЬБА для развития проекта считанные коды отправить на майл.
https://imgur.com/o3Lsz49.png
