#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>


// QSerialPort serial;



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    lg = new LG();
    serialptr = new QSerialPort();
    //read avaible comports
      foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
              ui->comboBox->addItem(info.portName());
   // timer = new QTimer();
    //connect(timer,&LG::pushButtonclicked,this,&MainWindow::changeWindow);
    //timer->start(1000); // 1000 ms
    connect(lg, &LG::sentData,this, &MainWindow::sendData);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::sendData( std::string str)
{
    //std::cout<<"ok"<<std::endl;
    serialptr->write(str.c_str());
}
//============================================
void MainWindow::on_pushButton_clicked()
{
    lg->show();
}
//===========================================
void MainWindow::on_pushButton_2_clicked()
{
    if (!serialptr->isDataTerminalReady()){
        if (serialptr->portName() != ui->comboBox->currentText())
        {   //serial.close();
            serialptr->setPortName(ui->comboBox->currentText());}
            ui->pushButton_2->setText("отключить");
            //setup COM port
               serialptr->setBaudRate(QSerialPort::Baud38400);
               serialptr->setDataBits(QSerialPort::Data8);
               serialptr->setParity(QSerialPort::NoParity);
               serialptr->setStopBits(QSerialPort::OneStop);
               serialptr->setFlowControl(QSerialPort::NoFlowControl);
               serialptr->open(QSerialPort::WriteOnly);
    }
    else{serialptr->close();
     ui->pushButton_2->setText("подключить");}

}
