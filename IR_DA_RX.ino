#include <iarduino_IR_RX.h>
//#include <iarduino_IR_TX.h>

iarduino_IR_RX IR(2);
void setup(){
  Serial.begin(9600);
  IR.begin();           
}
void loop(){
  //==========Чтение данных с любого пульта, реагируем только на нажатие кнопок:
 //  if(IR.check()){                                // Если в буфере имеются данные, принятые с пульта (была нажата кнопка)
 //   Serial.println(IR.data, HEX);                // Выводим код нажатой кнопки
 //   Serial.println(IR.length   );                // Выводим количество бит в коде
 // }
 //==========Чтение данных с любого пульта, реагируем на удержание кнопок:
  if(IR.check(true)){                            // Если в буфере имеются данные, принятые с пульта (удерживается кнопка)
    Serial.println(IR.data, HEX);                // Выводим код нажатой кнопки
    Serial.println(IR.length   );                // Выводим количество бит в коде
  }
}
