#include "lg.h"
#include "ui_lg.h"
#include <iostream>

LG::LG(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LG)
{
    ui->setupUi(this);
}

LG::~LG()
{
    delete ui;
}

//==ВКЛЮЧЕНИЕ
void LG::on_pushButton_2_clicked()
{   emit LG::sentData ("0x20DF10EF");}
//==energy saving
void LG::on_pushButton_3_clicked()
{   emit LG::sentData ("0x20DFA956");}
//==tv/ra
void LG::on_pushButton_clicked()
{   emit LG::sentData ("0x20DF0FF0");}
//==GUADE
void LG::on_pushButton_7_clicked()
{   emit LG::sentData ("0x20DFD52A");}
//==INFO
void LG::on_pushButton_6_clicked()
{   emit LG::sentData ("0x20DF55AA");}
//==RATIO
void LG::on_pushButton_5_clicked()
{   emit LG::sentData ("0x20DF9E61");}
//==INPUT
void LG::on_pushButton_4_clicked()
{   emit LG::sentData ("0x20DFD02F");}
//=="1"
void LG::on_pushButton_10_clicked()
{   emit LG::sentData ("0x20DF8877");}
//=="2"
void LG::on_pushButton_9_clicked()
{   emit LG::sentData ("0x20DF48B7");}
//=="3"
void LG::on_pushButton_8_clicked()
{   emit LG::sentData ("0x20DFC837");}
//=="4"
void LG::on_pushButton_11_clicked()
{   emit LG::sentData ("0x20DF28D7");}
//=="5"
void LG::on_pushButton_12_clicked()
{   emit LG::sentData ("0x20DFA857");}
//=="6"
void LG::on_pushButton_13_clicked()
{   emit LG::sentData ("0x20DF6897");}
//=="7"
void LG::on_pushButton_14_clicked()
{   emit LG::sentData ("0x20DFE817");}
//=="8"
void LG::on_pushButton_15_clicked()
{   emit LG::sentData ("0x20DF18E7");}
//=="9"
void LG::on_pushButton_16_clicked()
{   emit LG::sentData ("0x20DF9867");}
//=="0"
void LG::on_pushButton_17_clicked()
{   emit LG::sentData ("0x20DF08F7");}
//==settings
void LG::on_pushButton_21_clicked()
{   emit LG::sentData ("0x20DFC23D");}
//==Q.MENU
void LG::on_pushButton_20_clicked()
{   emit LG::sentData ("0x20DFA25D");}
//== назад
void LG::on_pushButton_19_clicked()
{   emit LG::sentData ("0x20DF14EB");}
//==EXIT
void LG::on_pushButton_18_clicked()
{   emit LG::sentData ("0x20DFDA25");}
//==VOL+
void LG::on_pushButton_27_clicked()
{   emit sentData ("0x20DF40BF");}
//==VOL-
void LG::on_pushButton_28_clicked()
{   emit LG::sentData ("0x20DFC03F");}
//==влево
void LG::on_pushButton_24_clicked()
{   emit LG::sentData ("0x20DFE01F");}
//==вправо
void LG::on_pushButton_25_clicked()
{   emit LG::sentData ("0x20DF609F");}
//==вверх
void LG::on_pushButton_23_clicked()
{   emit LG::sentData ("0x20DF02FD");}
//==вниз
void LG::on_pushButton_26_clicked()
{   emit LG::sentData ("0x20DF827D");}
//==OK
void LG::on_pushButton_22_clicked()
{   emit LG::sentData ("0x20DF22DD");}
//==P+
void LG::on_pushButton_29_clicked()
{   emit LG::sentData ("0x20DF00FF");}
//==P-
void LG::on_pushButton_30_clicked()
{   emit LG::sentData ("0x20DF807F");}
//==FAV
void LG::on_pushButton_32_clicked()
{   emit LG::sentData ("0x20DF7887");}
//==AD
void LG::on_pushButton_33_clicked()
{   emit LG::sentData ("0x20DF8976");}
//==MUTE
void LG::on_pushButton_31_clicked()
{   emit LG::sentData ("0x20DF906F");}
//==красный
void LG::on_pushButton_36_clicked()
{   emit LG::sentData ("0x20DF4EB1");}
//==зелёный
void LG::on_pushButton_35_clicked()
{   emit LG::sentData ("0x20DF8E71");}
//==желтый
void LG::on_pushButton_34_clicked()
{   emit LG::sentData ("0x20DFC639");}
//==синий
void LG::on_pushButton_37_clicked()
{   emit LG::sentData ("0x20DF8679");}
//==стоп
void LG::on_pushButton_39_clicked()
{   emit LG::sentData ("0x20DF8D72");}
//==воспр
void LG::on_pushButton_40_clicked()
{   emit LG::sentData ("0x20DF0DF2");}
//==пауза
void LG::on_pushButton_38_clicked()
{   emit LG::sentData ("0x20DF5DA2");}
//==субтитры
void LG::on_pushButton_41_clicked()
{   emit LG::sentData ("0x20DF9C63");}
//---------------------------------------
//==POWER_ONLY
void LG::on_pushButton_43_clicked()
{   emit LG::sentData ("0x20DF7F80");}
//==EZ_ADJ
void LG::on_pushButton_42_clicked()
{   emit LG::sentData ("0x20DFFF00");}
//==IN_START
void LG::on_pushButton_45_clicked()
{   emit LG::sentData ("0x20DFDF20");}
//==IN_STOP
void LG::on_pushButton_44_clicked()
{   emit LG::sentData ("0x20DF5FA0");}


